import { promises as fs } from "fs";
import path from "path";
import { parentPort, MessagePort } from "worker_threads";

parentPort.on("message", async data => {
	try {
		const { port }: { port: MessagePort } = data;
		const packagePath = path.join(path.resolve(), "./package.json");
		const content = await fs.readFile(packagePath);
		port.postMessage({ content: content.toString() });
	} catch (e) {
		console.error(e);
	}
});
